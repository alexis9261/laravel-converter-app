# Converter App

Este repositorio contiene la aplicacion que permite convertir distintas divisas.
La aplicacion fue desarrollada usando Laravel 10 y Livewire 3.

## Instalacion
```bash
composer install
npm install
php artisan key:generate
php artisan migrate --seed
```

## Ejecucion
```bash
php artisan serve
```

## Pruebas
```bash
php artisan test
```

## Despliegue
El despliegue se realizo en un servidor de Digital Ocean [Droplet], con las siguientes caracteristicas:
- Ubuntu 20.04 (LTS) x64
- 1 vCPU
- 1 GB / 25 GB Disk
- 1 TB Transfer

## Uso
Para usar la aplicacion, se debe ingresar a la ruta http://localhost:8000/

## Uso Publico
Para usar la aplicacion, se debe ingresar a la ruta https://alexlatam.tech/
Este dominio es personal y no tiene ningun fin comercial, solo es para mostrar la aplicacion funcionando.
La aplicacion cuenta con certificado SSL, por lo que se puede acceder a traves de https.

### La aplicacion tendra un tiempo de vida de 1 semana, desde el 26/11/2023 hasta el 02/12/2023 
