<x-guest-layout>
    <div class="py-12 bg-gray-100 min-h-screen">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="col-span-4">
                <!-- main card -->
                <div class="flex items-center justify-center bg-red-200">
                    <div class="w-full rounded-lg bg-white p-3 drop-shadow-xl divide-y divide-gray-200" >
                        <div aria-label="header" class="flex space-x-4 items-center p-4 font-bold text-blue-950 justify-between">
                            <span> Exchange For Software Engineers </span>
                            @if(Auth::user())
                                <span class="text-red-600">Haz consumido los cinco [5] usos*</span>
                            @endif
                        </div>
                        <div class="pt-10 px-6">
                            <!-- Results -->
                            <div>
                                <div class="text-lg text-semibold text-gray-700">
                                    Nos entristece informarte que haz consumido los cinco [5] usos gratuitos que tenías disponibles.
                                </div>
                                <div class="text-3xl text-bold">
                                    Te invitamos a registrarte en nuestra plataforma y disfrutar de todos los beneficios que tenemos para ti.
                                </div>
                                <div class="text-lg text-light text-gray-700">
                                    Hazlo! es gratis.
                                </div>
                                <div class="my-6">
                                    <a class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" href="{{route('register')}}">Registrarme</a>
                                </div>
                            </div>
                            <!-- Bottom info -->
                            <div class="text-end mb-10">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-guest-layout>
