<x-guest-layout>
    <div class="py-12 bg-gray-100 min-h-screen">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="col-span-4">
                <!-- main card -->
                <div class="flex items-center justify-center">
                    <div class="w-full rounded-lg bg-white p-3 drop-shadow-xl divide-y divide-gray-200" >
                        Upps! Algo salió mal. Por favor, inténtelo de nuevo más tarde.
                        Tuvimos una pequeña falla en el sistema.
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-guest-layout>
