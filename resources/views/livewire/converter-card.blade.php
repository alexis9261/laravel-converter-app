<div>
    <div aria-label="header" class="flex space-x-4 items-center p-4 font-bold text-blue-950 justify-between">
        <span>Exchange For Software Engineers </span>
        @if(Auth::user())
            <span class="text-green-600">Usos ilimitados</span>
        @else
            <span>{{$attempts}} usos restantes*</span>
        @endif
    </div>
    <div class="pt-10 px-6">
        <!-- component -->
        <div class="grid grid-cols-1 md:grid-cols-10 gap-4 mb-8">
            <div class="col-span-3 mb-6 md:mb-0">
                <label class="block tracking-wide text-grey-darker font-bold mb-2" for="grid-first-name">
                    Importe
                </label>
                <input
                    class="appearance-none block w-full bg-grey-lighter text-grey-darker border border-red rounded py-3 px-4 mb-3"
                    id="grid-first-name"
                    type="number"
                    placeholder="1,00"
                    min="1"
                    step="0.01"
                    wire:model.lazy="amount"
                >
                <p class="text-xs text-red-600">&nbsp
                    @error('amount')Introduzca una cantidad válida.@enderror
                </p>
            </div>
            <div class="col-span-3 mb-6 md:mb-0">
                <label class="block tracking-wide text-grey-darker font-bold mb-2" for="grid-state">
                    De:
                </label>
                <div class="relative">
                    <select
                        class="block appearance-none w-full bg-grey-lighter border border-grey-lighter text-grey-darker py-3 px-4 pr-8 rounded"
                        id="grid-state"
                        wire:model.lazy="from"
                    >
                        @foreach($currencies as $currency)
                            <option value="{{$currency->code}}-{{$currency->title}}" wire:key="{{ $currency->code }}">{{$currency->code}} - {{$currency->title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-span-1 mb-6 md:mb-0 place-self-center">
                <img
                    class="rounded-full border border-black h-10 p-2 cursor-pointer"
                    src="{{asset('images/reverse.svg')}}"
                    alt="inverter"
                    wire:click="reverse"
                >
            </div>
            <div class="col-span-3 mb-6 md:mb-0">
                <label class="block tracking-wide text-grey-darker font-bold mb-2" for="grid-state">
                    Hacia:
                </label>
                <div class="relative">
                    <select
                        class="block appearance-none w-full bg-grey-lighter border border-grey-lighter text-grey-darker py-3 px-4 pr-8 rounded"
                        id="grid-state"
                        wire:model.lazy="to"
                    >
                        @foreach($currencies as $currency)
                            <option value="{{$currency->code}}-{{$currency->title}}" wire:key="{{ $currency->code }}">{{$currency->code}} - {{$currency->title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <!-- Results -->
        <div>
            <div class="text-lg text-semibold text-gray-700">
                {{$amount}} {{explode('-', $from)[1]}} =
            </div>
            <div class="text-3xl text-bold">
                {{$result}} {{explode('-', $to)[1]}}
            </div>
            <div class="text-lg text-light text-gray-700">
                {{$amount}} {{explode('-', $from)[1]}} = {{$result}} {{explode('-', $to)[1]}}
            </div>
        </div>
        <!-- Bottom info -->
        <div class="grid grid-cols-1 md:grid-cols-2 gap-6">
            <div class="px-4 py-2 bg-blue-50 rounded-lg my-6 text-sm">
                Usamos la tasa del mercado medio para nuestro conversor.
                Esto solo tiene fines informativos.
                No recibirás esta tasa cuando envíes dinero.
                <span class="text-blue-700 cursor-pointer">Consulta las tasas de envío.</span>
            </div>
        </div>
        <div class="text-end mb-10">
            <p class="text-gray-700 text-xs">
                Conversión de <span class="text-blue-700 cursor-pointer">{{explode('-', $from)[1]}}</span> a
                <span class="text-blue-700 cursor-pointer">{{explode('-', $to)[1]}}</span> — Última actualización: {{now()->format('d/m/Y')}}
            </p>
            @if(!Auth::user())
                <p class="text-sm">
                    Solo tienes cinco usos gratuitos de este conversor de divisas.
                    <a class="text-blue-700" href="{{route('register')}}">Registrate</a> y podras usarlo ilimitadamente.
                </p>
            @endif
        </div>
    </div>
</div>
