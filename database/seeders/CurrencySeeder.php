<?php

namespace Database\Seeders;

use App\Contracts\CurrencyInterface;
use App\Models\Currency;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

final class CurrencySeeder extends Seeder
{
    private array $currencies;

    public function __construct(CurrencyInterface $currency)
    {
        $this->currencies = $currency->getCurrencies();
    }
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach ($this->currencies as $key => $value) {
            Currency::factory([
                'title' => $value,
                'code' => $key,
            ])->create();
        }
    }
}
