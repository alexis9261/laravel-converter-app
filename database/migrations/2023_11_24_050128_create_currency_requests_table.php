<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('currency_requests', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->default(1)->constrained();
            $table->string('from');
            $table->string('to');
            $table->float('amount');
            $table->float('result');
            $table->string('ip');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('currency_requests');
    }
};
