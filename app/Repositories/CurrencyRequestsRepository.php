<?php

namespace App\Repositories;

use App\DTO\CurrencyRequestDto;
use App\Models\CurrencyRequest;
use App\Repositories\Contracts\CurrencyRequestsRepositoryInterface;

final class CurrencyRequestsRepository implements CurrencyRequestsRepositoryInterface
{
    public function all(int $user_id): array
    {
        return CurrencyRequest::all()->where('user_id', $user_id)->toArray();
    }

    public function save(CurrencyRequestDto $data): void
    {
        $currencyRequest = new CurrencyRequest();
        $currencyRequest->create($data->toArray());
    }
}
