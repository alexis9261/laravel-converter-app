<?php

namespace App\Repositories\Contracts;

use App\DTO\CurrencyRequestDto;

interface CurrencyRequestsRepositoryInterface
{
    public function all(int $user_id): array;
    public function save(CurrencyRequestDto $data): void;
}
