<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

final class DashboardController extends Controller
{
    public function __invoke(): View
    {
        return view('dashboard', [
            'transactions' => Auth::user()->currencyRequests,
        ]);
    }
}
