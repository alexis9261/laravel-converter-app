<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;

final class AttemptsController extends Controller
{
    public function __invoke(): View
    {
        return view('attempts');
    }
}
