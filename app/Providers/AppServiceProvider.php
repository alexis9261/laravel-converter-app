<?php

namespace App\Providers;

use App\Contracts\CurrencyInterface;
use App\Contracts\SessionInterface;
use App\Repositories\Contracts\CurrencyRequestsRepositoryInterface;
use App\Repositories\CurrencyRequestsRepository;
use App\Services\CurrencyLayer;
use App\Services\SessionService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public $singletons = [
        CurrencyInterface::class => CurrencyLayer::class,
        CurrencyRequestsRepositoryInterface::class => CurrencyRequestsRepository::class,
        SessionInterface::class => SessionService::class,
    ];

    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
