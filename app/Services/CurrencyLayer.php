<?php

namespace App\Services;

use App\Contracts\CurrencyInterface;
use App\Exceptions\GetConvertCurrencyException;
use App\Exceptions\GetCurrencyListException;
use Illuminate\Support\Facades\Http;

final class CurrencyLayer implements CurrencyInterface
{
    private string $base_url;
    private string $access_key;

    public function __construct()
    {
        $this->base_url = config('services.currency_layer.base_url');
        $this->access_key = config('services.currency_layer.access_key');
    }

    /**
     * @throws GetCurrencyListException
     */
    public function getCurrencies(): array
    {
        $result = Http::get($this->base_url . '/list?access_key=' . $this->access_key)->json();

        if (isset($result['error'])){
            throw new GetCurrencyListException($result['error']['info']);
        }

        return $result['currencies'];
    }

    /**
     * @throws GetConvertCurrencyException
     */
    public function convert(string $from, string $to, float $amount): float
    {
        $url = $this->base_url.'/convert?from='.$from.'&to='.$to.'&amount='.$amount.'&access_key='.$this->access_key;
        $result = Http::get($url)->json();

        if (isset($result['error'])){
            throw new GetConvertCurrencyException($result['error']['info']);
        }

        return $result['result'];
    }
}
