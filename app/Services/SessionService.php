<?php

namespace App\Services;

use App\Contracts\SessionInterface;
use Illuminate\Support\Facades\Session;

final class SessionService implements SessionInterface
{
    public function set(string $key, mixed $value): void
    {
        Session::put($key, $value);
    }

    public function exists(string $key): bool
    {
        return Session::has($key);
    }

    public function get(string $key): float
    {
        return Session::get($key);
    }
}
