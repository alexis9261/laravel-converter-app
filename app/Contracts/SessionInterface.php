<?php

namespace App\Contracts;

interface SessionInterface
{
    public function set(string $key, mixed $value): void;
    public function exists(string $key): bool;
    public function get(string $key): float;
}
