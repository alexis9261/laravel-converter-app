<?php

namespace App\Contracts;

interface CurrencyInterface
{
    public function getCurrencies(): array;
    public function convert(string $from, string $to, float $amount): float;
}
