<?php

namespace App\DTO;

final readonly class CurrencyRequestDto
{
    private const DEFAULT_USER_ID = 1;

    private function __construct(
        private ?int   $userId,
        private string $from,
        private string $to,
        private float  $amount,
        private float  $result,
        private string $ip,
    ) {
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function getFrom(): string
    {
        return $this->from;
    }

    public function getTo(): string
    {
        return $this->to;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getResult(): float
    {
        return $this->result;
    }

    public function getIp(): string
    {
        return $this->ip;
    }

    public static function create(
        ?int   $userId,
        string $from,
        string $to,
        float  $amount,
        float  $result,
        string $ip,
    ): self
    {
        return new self(
            userId: $userId ?? self::DEFAULT_USER_ID,
            from: $from,
            to: $to,
            amount: $amount,
            result: $result,
            ip: $ip,
        );
    }

    public function toArray(): array
    {
        return [
            'user_id' => $this->userId,
            'from' => $this->from,
            'to' => $this->to,
            'amount' => $this->amount,
            'result' => $this->result,
            'ip' => $this->ip,
        ];
    }
}
