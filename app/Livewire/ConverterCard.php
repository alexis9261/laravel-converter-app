<?php

namespace App\Livewire;

use App\Contracts\CurrencyInterface;
use App\Contracts\SessionInterface;
use App\DTO\CurrencyRequestDto;
use App\Models\Currency;
use App\Repositories\Contracts\CurrencyRequestsRepositoryInterface;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

final class ConverterCard extends Component
{
    private const INITIAL_CURRENCY_CURRENT = 'USD-United States Dollar';
    private const INITIAL_CURRENCY_TARGET = 'EUR-Euro';
    private const INITIAL_AMOUNT = 1;
    private const MAX_ATTEMPTS = 5;

    private CurrencyInterface $currencyService;
    private SessionInterface $session;
    private CurrencyRequestsRepositoryInterface $currencyRequestRepository;

    public string $from = self::INITIAL_CURRENCY_CURRENT;
    public string $to = self::INITIAL_CURRENCY_TARGET;
    public float $amount = self::INITIAL_AMOUNT;
    public int $attempts = self::MAX_ATTEMPTS;
    public float $result = 0.0;
    public array $requests = [];

    public function rules(): array
    {
        return [
            'from' => 'required',
            'to' => 'required',
            'amount' => 'required|numeric|min:0.01',
        ];
    }

    public function mount(): void
    {
        $this->validateAttempts();
        $this->convert();
    }

    public function boot(
        SessionInterface $session,
        CurrencyInterface $currencyService,
        CurrencyRequestsRepositoryInterface $currencyRequestRepository
    ): void
    {
        $this->session = $session;
        $this->currencyService = $currencyService;
        $this->currencyRequestRepository = $currencyRequestRepository;
    }

    public function render(): View
    {
        return view('livewire.converter-card', [
            'currencies' => Currency::all(),
        ]);
    }

    public function reverse(): void
    {
        $aux = $this->from;
        $this->from = $this->to;
        $this->to = $aux;
        $this->convert();
        $this->validateAttempts();
    }

    public function updated($value): void
    {
        $this->convert();
        $this->validateAttempts();
    }

    public function convert(): void
    {
        $this->validate();

        $from_code = explode('-', $this->from)[0];
        $to_code = explode('-', $this->to)[0];

        try {
            $this->result = $this->currencyService->convert(
                $from_code,
                $to_code,
                $this->amount
            );

            $currency_request = CurrencyRequestDto::create(
                userId: Auth::id(),
                from: $from_code,
                to: $to_code,
                amount: $this->amount,
                result: $this->result,
                ip: request()->ip()
            );
            $this->currencyRequestRepository->save($currency_request);

        } catch ( \Exception $e) {
        }

    }

    private function validateAttempts(): void
    {
        if(!is_null(Auth::user())) {
            return;
        }

        if (!$this->session->exists('attempts')) {
            $this->session->set('attempts', $this->attempts);
            return;
        }

        if ($this->session->get('attempts') == 0) {
            redirect()->route('attempts');
        }

        $this->attempts -= 1;
        $this->session->set('attempts', $this->attempts);
        if ($this->attempts === 0) {
            redirect()->route('attempts');
        }
    }
}
