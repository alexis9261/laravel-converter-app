<?php

use App\Http\Controllers\AttemptsController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;
use App\Services\CurrencyLayer;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', HomeController::class)->name('home');

Route::get('/maximos-intentos', AttemptsController::class)->name('attempts');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', DashboardController::class)->name('dashboard');
});
