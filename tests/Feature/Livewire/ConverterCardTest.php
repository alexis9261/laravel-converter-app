<?php

namespace Tests\Feature\Livewire;

use App\Livewire\ConverterCard;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ConverterCardTest extends TestCase
{
    use RefreshDatabase;

    public function test_converter_card_component_is_rendered(): void
    {
        $this->get('/')
            ->assertSeeLivewire(ConverterCard::class);
    }
}
